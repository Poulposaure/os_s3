// TP 2 SYNC
// REPAIN Paul
// DUT INFO
//
//
// to compile :
// gcc -Wall -lpthread tp_thread.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <semaphore.h>




/************************************/
/*                                  */
/*            CONSTANTS             */
/*                                  */
/************************************/

#define MAX 5
#define P(x) sem_wait(x)
#define V(x) sem_post(x)
#define N 100
#define DEBUG printf("**%d**\n", __LINE__);


/************************************/
/*                                  */
/*            PROTOTYPES            */
/*                                  */
/************************************/

void question11();
void* fct_thread();


void question12();
void* first_thread();
void* second_thread();


void question12_mutexv();
void* first_thread_mutexv();
void* second_thread_mutexv();


void question13();
void *random_wait();
void *barrier_thread();


void question14();
void *write_buffer(void *args);
void *read_buffer();



/************************************/
/*                                  */
/*         GLOBAL VARIABLES         */
/*                                  */
/************************************/

int g_globale = 0;

sem_t semaphore;
sem_t semaphore2;
pthread_mutex_t mutex;
pthread_mutex_t mutex2;
pthread_cond_t condition;

char buffer[N];



/************************************/
/*                                  */
/*               MAIN               */
/*                                  */
/************************************/

int main()
{
    //question11();
    //question12();
    question12_mutexv();
    //question13();
    //question14();

    return 0;
}



/************************************/
/*                                  */
/*            DEFINITIONS           */
/*                                  */
/************************************/

void question11()
{
    int i;
    pthread_t threads[MAX];

    // creation of threads
    for (i = 0; i != MAX; i++)
    {
        pthread_create((threads + i), NULL, fct_thread, NULL);
    }

    // the pthread_join function waits for the thread to terminate
    for (i = 0; i != MAX; i++)
    {
        pthread_join(*(threads + i), NULL);
    }

    pthread_exit(NULL);

    return;
}

void* fct_thread()
{
    int l_locale = 0;

    printf("** %d %d **\n", g_globale--, l_locale++);

    return NULL;
}


void question12()
{
    pthread_t threads[2];
    // Arguments de sem_init() :
    // 1 : address of the semaphore ; 2 : 0 if semaphore shared between threads
    // of a same process ; 3 : value of semaphore
    sem_init(&semaphore, 0, 0);
    sem_init(&semaphore2, 0, 0);

    pthread_create((threads + 0), NULL, first_thread, NULL);
    pthread_create((threads + 1), NULL, second_thread, NULL);
    // pthread_join : wait for the thread to execute
    pthread_join(*(threads + 0), NULL);
    pthread_join(*(threads + 1), NULL);

    sem_destroy(&semaphore);
    sem_destroy(&semaphore2);
    pthread_exit(NULL);

    return;
}

void* first_thread()
{
    puts("Je");
    V(&semaphore2);
    P(&semaphore);
    puts("mes");
    V(&semaphore2);

    return NULL;
}

void* second_thread()
{
    P(&semaphore2);
    puts("synchronise");
    V(&semaphore);
    P(&semaphore2);
    puts("threads");

    return NULL;
}



void question12_mutexv()
{
    pthread_t threads[2];
    pthread_mutex_init(&mutex, NULL);
    pthread_mutex_init(&mutex2, NULL);

    //mutex = PTHREAD_MUTEX_INITIALIZER;// error
    //mutex2 = PTHREAD_MUTEX_INITIALIZER;// error

    pthread_mutex_lock(&mutex);
    pthread_mutex_lock(&mutex2);

    pthread_create((threads + 0), NULL, first_thread_mutexv, NULL);
    pthread_create((threads + 1), NULL, second_thread_mutexv, NULL);
    pthread_join(*(threads + 0), NULL);
    pthread_join(*(threads + 1), NULL);

    pthread_mutex_destroy(&mutex);
    pthread_mutex_destroy(&mutex2);
    pthread_exit(NULL);

    return;
}


void* first_thread_mutexv()
{
    printf("Je ");
    pthread_mutex_unlock(&mutex2);
    pthread_mutex_lock(&mutex);

    printf("mes ");
    pthread_mutex_unlock(&mutex2);

    return NULL;
}

void* second_thread_mutexv()
{
    pthread_mutex_lock(&mutex2);

    printf("synchronise ");
    pthread_mutex_unlock(&mutex);
    pthread_mutex_lock(&mutex2);

    puts("threads");

    return NULL;
}



void question13()
{
    int i;
    pthread_t threads[MAX];

    sem_init(&semaphore, 0, 0);
    pthread_create((threads +0), NULL, barrier_thread, NULL);

    for (i = 1; i != MAX; i++)
    {
        pthread_create((threads + i), NULL, random_wait, NULL);
    }

    for (i = 0; i != MAX; i++)
    {
        pthread_join(threads[i], NULL);
    }

    sem_destroy(&semaphore);
    pthread_exit(NULL);

    return;
}

void *random_wait()
{
    int wait = abs(rand())%10;

    // each thread will have to wait randomly
    sleep(wait);
    printf("[thread %d] Point atteint\n", getpid());
    V(&semaphore);

    return NULL;
}

void *barrier_thread()
{
    int i;

    puts("[b_thread] Barriere atteinte, en attente...");
    // the barrier thread has to wait for all the other threads
    for (i = 1; i != MAX; i++)
    {
        P(&semaphore);
    }
    // the other threads unlocked the semaphore
    puts("[b_thread] Je peux continuer !");

    return NULL;
}

void question14()
{
    pthread_t threads[2];
    char args[] = "Une ecriture";

    sem_init(&semaphore, 0, 0);
    sem_init(&semaphore2, 0, 0);
    // one thread to read the buffer
    // one thread to write in the buffer

    pthread_create((threads + 0), NULL, write_buffer, (void *) args);
    pthread_create((threads + 1), NULL, read_buffer, NULL);
    pthread_join(threads[0], NULL);
    pthread_join(threads[1], NULL);

    sem_destroy(&semaphore);
    sem_destroy(&semaphore2);
    pthread_exit(NULL);

    return;
}



void *write_buffer(void *args)
{
    // add the message into the buffer
    strcpy(buffer, args);
    // we can read again
    V(&semaphore);

    return NULL;
}

void *read_buffer()
{
    // can't read till we didn't write in the buffer
    P(&semaphore);
    printf("Je lis : %s\n", buffer);
    V(&semaphore2);

    return NULL;
}
