**TP en Systèmes d'Exploitation**


*  TP - Gestion de processus :
*fork(), exec(), pid_t*

*  TP - Synchronisation :
*pthread_create(), pthread_join(), pthread_exit(), pthread_t*
*sem_init(), sem_wait(), sem_post(), sem_destroy(), sem_t*
*pthread_mutex_init(), pthread_mutex_unlock(), pthread_mutex_lock(), pthread_mutex_destroy(), pthread_mutex_t*

*  TP - IPC :
*pipe(), dup(), dup2(), open(), creat(), read(), write(), close(), STDIN_FILENO, STDOUT_FILENO*
