// TP4 IPC
// Repain Paul
// 2019

#include <stdio.h>// perror, printf
#include <unistd.h>// dup, dup2
#include <stdlib.h>// EXIT_FAILURE, EXIT_SUCCESS
#include <fcntl.h>// creat



#define DEBUG printf("<%d>\n", __LINE__);

#define WAY 4


// to compile : gcc -Wall redirection_stdtout.c
// to execute : ./a.out test.txt



// equivalent to > (overwriting) or >> (appending), it depends on the flags
// you're using for open (or creat)
// echo "Après redirection" > test.txt

int main(int argc, char *argv[])
{
    char *filename = argv[1];
    int f;


    printf("Avant redirection\n");



    #if WAY == 1
    // the order matters
    // we close the stdout
    close(STDOUT_FILENO);

    // and we open a new file descriptor, the file passed during the execution
    // dup2 takes the smallest fildes available
    f = creat(filename, S_IRWXU);
    //f = open(filename, O_CREAT|O_WRONLY|O_APPEND, S_IRWXU);
    // we notice the file became the new stdout, because the fd is now vacant
    //printf("FILEDES %d STDOUT_FILENO %d\n", f, fileno(stdout));
    #endif



    #if WAY == 2
    f = creat(filename, S_IRWXU);

    // closing makes the filedes 1 free to use
    close(STDOUT_FILENO);

    int f2 = dup(f);
    // we notice the file became the new stdout, because the fd is now vacant
    //printf("FILEDES2 %d STDOUT_FILENO %d\n", f2, fileno(stdout));
    #endif



    #if WAY == 3
    // Another way to do it :
    // we redirect the standard output into the file passed during the call
    freopen(filename, "w", stdout);
    #endif



    #if WAY == 4
    f = creat(filename, S_IRWXU);

    dup2(f, STDOUT_FILENO);

    close(f);
    #endif



    #if WAY == 5
    #endif



    printf("Après redirection\n");


    return EXIT_SUCCESS;
}

