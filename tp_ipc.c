// TP4 Communication InterProcessus IPC
// REPAIN Paul
// 2018

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <time.h>


/*************************************************/
/*                                               */
/*      MACRO CONSTANTS AND MACRO FUNCTIONS      */
/*                                               */
/*************************************************/
#define MAX_SIZE 100
#define DEBUG printf("<%d>\n", __LINE__);


/*************************************************/
/*                                               */
/*             FUNCTIONS PROTOTYPES              */
/*                                               */
/*************************************************/
void question41();

void question43();
void write_v1(char *filename, char text[]);
void read_v1(char *filename);
void copy_v1(char *filename, char *filename2);

void question44();
void write_v2(char *filename, char text[]);
void read_v2(char *filename);
void copy_v2(char *filename, char *filename2);

void question45();

void question46();
void simuler_tube_shell(char *cmd1, char *cmd2);
void simuler_tube_shell_v2(char *cmd1, char *const argv1[], char *cmd2, char *const argv2[]);


/*************************************************/
/*                                               */
/*                     MAIN                      */
/*                                               */
/*************************************************/
int main(int argc, char *argv[])
{
    system("clear");

    //question41();
    //question43();
    //question44();
    //question45();
    question46();

    return EXIT_SUCCESS;
}


/*************************************************/
/*                                               */
/*           DEFINITIONS OF FUNCTIONS            */
/*                                               */
/*************************************************/


/*************************************************/
/* question41                                    */
/*                                               */
/* Displays the number of the 3 files descriptor */
/*************************************************/
void question41()
{
    printf("STDIN_FILENO : %d\n", fileno(stdin));// standard input 0
    // cat /usr/include/*.h | grep "standard input"
    printf("STDOUT_FILENO : %d\n", fileno(stdout));// standard output 1
    // cat /usr/include/*.h | grep "standard output"
    printf("STDERR_FILENO : %d\n", fileno(stderr));// standard error 2
    // cat /usr/include/*.h | grep "standard error"
    // Also :
    // cat /usr/include/unistd.h | grep "standard"
    // Or :
    // cat /usr/include/unistd.h | grep "Standard"
    // 3, 4, ..., 9 are for additional files

    return;
}


/*************************************************/
/* question43                                    */
/*                                               */
/* read files, write in files, copy files        */
/* using the functions fopen fgets fputs fclose  */
/*************************************************/
void question43()
{
    char *filename = "test.txt";
    char *filename2 = "test2.txt";
    char text[] = "DU TEXTE";

    write_v1(filename, text);
    read_v1(filename);
    copy_v1(filename, filename2);
    read_v1(filename2);

    return;
}


/*************************************************/
/* write_v1                                      */
/*                                               */
/* write in file, which name is filename, text   */
/*                                               */
/* IN : name of file, text to write in file      */
/* OUT : void                                    */
/*************************************************/
void write_v1(char *filename, char text[])
{
    FILE *file = NULL;

    // if the text is empty string, it is useless to write it
    if (strcmp(text, "") == 0) return;

    file = fopen(filename, "a");

    if (file != NULL) fputs(text, file);

    fclose(file);

    return;
}


/*************************************************/
/* read_v1                                       */
/*                                               */
/* read a file, which name is filename, and      */
/* display the result                            */
/*                                               */
/* IN : name of file                             */
/* OUT : void                                    */
/*************************************************/
void read_v1(char *filename)
{
    char text[MAX_SIZE] = "";

    FILE *file = fopen(filename, "r");

    if (file != NULL)
    {
        while (!feof(file))
        {
            fgets(text, sizeof(text), file);
            printf("%s\n", text);
        }
    }

    fclose(file);

    return;
}


/*************************************************/
/* copy_v1                                       */
/*                                               */
/* read a file, and copy the content in the      */
/* second file passed in the arguments           */
/*                                               */
/* IN : name of file 1, name of file 2           */
/* OUT : void                                    */
/*************************************************/
void copy_v1(char *filename, char *filename2)
{
    char text[MAX_SIZE] = "";
    FILE *file = NULL, *file2 = NULL;

    file = fopen(filename, "r");
    file2 = fopen(filename2, "a");

    if (file == NULL || file2 == NULL)
    {
        perror("Open file failed");
        return;
    }

    while (!feof(file))
    {
        fgets(text, sizeof(text), file);
        fputs(text, file2);
    }

    fclose(file);
    fclose(file2);

    return;
}


/*************************************************/
/* question44                                    */
/*                                               */
/* read files, write in files, copy files        */
/* using the system call open read write         */
/*************************************************/
void question44()
{
    char *filename = "test.txt";
    char *filename2 = "test3.txt";
    char text[] = "DU TEXTE";

    write_v2(filename, text);
    read_v2(filename);
    copy_v2(filename, filename2);
    read_v2(filename2);

    return;
}


/*************************************************/
/* write_v2                                      */
/*                                               */
/* write in file, which name is filename, text   */
/*                                               */
/* IN : name of file, text to write in file      */
/* OUT : void                                    */
/*************************************************/
void write_v2(char *filename, char text[])
{
    int filedesc = creat(filename, S_IRWXU);

    if (filedesc == -1) return;

    write(filedesc, (void *) text, sizeof(text));

    close(filedesc);

    return;
}


/*************************************************/
/* read_v2                                       */
/*                                               */
/* read a file, which name is filename, and      */
/* display the result                            */
/*                                               */
/* IN : name of file                             */
/* OUT : void                                    */
/*************************************************/
void read_v2(char *filename)
{
    char text[MAX_SIZE] = "";
    // O_RDONLY : read only
    // O_CREAT : create the file if it does not exist
    // S_IRWXU : user can read write execute; can also write 00700
    int filedesc = open(filename, O_RDONLY|O_CREAT, S_IRWXU);

    if (filedesc == -1)
    {
        perror("Open file failed");
        return;
    }

    read(filedesc, (void *) text, sizeof(text));
    printf("%s\n", text);

    close(filedesc);

    return;
}


/*************************************************/
/* copy_v2                                       */
/*                                               */
/* read a file, and copy the content in the      */
/* second file passed in the arguments           */
/*                                               */
/* IN : name of file 1, name of file 2           */
/* OUT : void                                    */
/*************************************************/
void copy_v2(char *filename, char *filename2)
{
    char text[MAX_SIZE] = "";// the initialization avoids unwanted characters
    int filedesc, filedesc2;

    filedesc = open(filename, O_RDONLY|O_CREAT, S_IRWXU);
    filedesc2 = creat(filename2, S_IRWXU);

    if (filedesc == -1 || filedesc2 == -1)
    {
        perror("Open file failed");
        return;
    }

    read(filedesc, (void *) text, sizeof(text));
    write(filedesc2, (void *) text, sizeof(text));

    close(filedesc);
    close(filedesc2);

    return;
}


/*
 * open, read, write et close sont des appels systèmes.
 *
 * Les appels systèmes sont des interfaces entre un processus
 * et un système d'exploitation. Autrement dit : un service est
 * demandé au noyau.
 *
 * Ils appellent la bibliothèque vDSO.
 * vDSO (Virtual Dynamic Shared Object) : mécanisme d'exports
 * d'un ensemble de routines noyau dans l'espace utilisateur.
 * 
 *
 * Dans le noyau, l'appel est d'abord géré par le vfs
 * (Virtual File System ou systèmes de fichiers virtuels)
 *
 */







/*************************************************/
/* question45                                    */
/*                                               */
/* the process has to guess the mystery number   */
/* which is a random number generated by the     */
/* father. Needs 2 pipes : one to send the child */
/* input, one to send the boolean indicating     */
/* if the child is right                         */
/* The boolean is between min and max            */
/*                                               */
/* IN : void                                     */
/* OUT : void                                    */
/*************************************************/
void question45()
{
    int max = 1000, min = 100, fd[2], fd2[2], input = 0, success = 1, random = 1;

    if (pipe(fd) == -1 || pipe(fd2) == -1)
    {
        perror("Pipe failed");
        return;
    }

    switch (fork())
    {
        case -1:// error
            perror("Fork failed");
            return;

        case 0:// child
            close(fd[0]);// close writing
            close(fd2[1]);// close reading

            do
            {
                // the child has to guess the random number
                scanf(" %d", &input);

                // the father will see if the input is right and send a message
                write(fd[1], &input, sizeof(input));

                // the child see if he is right
                read(fd2[0], &success, sizeof(success));
            } while (success != 0);

            close(fd[1]);
            close(fd2[0]);

            exit(EXIT_SUCCESS);

        default:// father
            close(fd[1]);// close reading
            close(fd2[0]);// close writing

            // initialize the random / mystery number, with the seed
            srand(time(NULL));
            random = (rand()%(max - min)) + min;
            //printf("(%d)\n", random);

            do
            {
                // the father indicates to the child the number guessed is
                // wrong ; the instruction mustn't be outside the loop !
                write(fd2[1], &success, sizeof(success));

                puts("[pere]Entrez un nombre entre 100 et 1000");

                // the father see what the child input is
                read(fd[0], &input, sizeof(input));

                // the father gives the child a clue if he is wrong
                if (input > random)
                    puts("Trop grand");
                else if (input < random)
                    puts("Trop petit");
            } while (input != random);

            // the father tells the child he guessed right
            success = 0;
            write(fd2[1], &success, sizeof(success));
            puts("Bravo !");

            close(fd[0]);
            close(fd2[1]);
    }// switch

    return;
}


/*************************************************/
/* question46                                    */
/*                                               */
/* Imitates the shell pipe for two bash commands */
/* One version without options (system() call)   */
/* One version with options (exec() call)        */
/*                                               */
/* IN : void                                     */
/* OUT : void                                    */
/*************************************************/
void question46()
{
    char *argv1[] = {"ls", NULL};
    //char *argv2[] = {"grep", ".c", "-c", NULL};
    char *argv2[] = {"wc", NULL};

    simuler_tube_shell("ls", "wc");
    //simuler_tube_shell_v2("ls", argv1, "wc", argv2);

    return;
}


/*************************************************/
/* simuler_tube_shell                            */
/*                                               */
/* Imitates the shell pipe for two bash commands */
/* Without options (system() call)               */
/* cmd1 | cmd2                                   */
/*                                               */
/* IN : 2 strings for the 2 bash commands        */
/* OUT : void                                    */
/*************************************************/
void simuler_tube_shell(char *cmd1, char *cmd2)
{
    int fd[2];

    if (pipe(fd) == -1) return;

    switch (fork())
    {
        case -1:
            perror("Fork failed");
            return;

        case 0:// child
            close(fd[1]);

            // we link the stdin
            dup2(fd[0], STDIN_FILENO);

            system(cmd2);

            close(fd[0]);

            exit(EXIT_SUCCESS);

        default:// father
            close(fd[0]);

            // we link the stdout to fd
            dup2(fd[1], STDOUT_FILENO);

            system(cmd1);

            close(fd[1]);
            close(STDOUT_FILENO);// process stops if not

            wait(NULL);
    }// switch

    return;
}


/*************************************************/
/* simuler_tube_shell_v2                         */
/*                                               */
/* Imitates the shell pipe for two bash commands */
/* With options (exec() call)                    */
/* cmd1 | cmd2                                   */
/*                                               */
/* IN : 2 strings for the 2 bash commands        */
/*      2 arrays of arguments for each command   */
/* OUT : void                                    */
/*************************************************/
void simuler_tube_shell_v2(char *cmd1, char *const argv1[], char *cmd2, char *const argv2[])
{
    int fd[2];

    if (pipe(fd) != 0) return;

    switch (fork())
    {
        case -1:
            perror("Fork failed");
            return;

        case 0:// child
            close(fd[1]);

            // we link the stdin
            if (dup2(fd[0], STDIN_FILENO) == -1)
            {
                perror("Duplication of stdin failed");
                return;
            }

            //execlp(cmd2, cmd2, NULL);
            if (execvp(cmd2, argv2) == -1)
            {
                perror("System call of second command failed");
                return;
            }

            close(fd[0]);
            exit(EXIT_SUCCESS);

        default:// father
            close(fd[0]);

            // we link the stdout to fd
            if (dup2(fd[1], STDOUT_FILENO) == -1)
            {
                perror("Duplication of stdout failed");
                return;
            }

            //execlp(cmd1, cmd1, NULL);
            // if for readability purpose
            // the process continue if exec fails
            if (execvp(cmd1, argv1) == -1)
            {
                perror("System call of first command failed");
                return;
            }

            close(fd[1]);
            wait(NULL);
    }// switch

    return;
}

