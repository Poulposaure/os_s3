// TP 1 PROCESSUS
// REPAIN Paul
// DUT INFO

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


/************************************/
/*                                  */
/*            CONSTANTS             */
/*                                  */
/************************************/

#define NB_TOURS 2


/************************************/
/*                                  */
/*            PROTOTYPES            */
/*                                  */
/************************************/

void question1();
void question21();
void question22(char *argv);
void question24(char *argv);
void question26();


/************************************/
/*                                  */
/*         GLOBAL VARIABLES         */
/*                                  */
/************************************/

int globale = 1;


/************************************/
/*                                  */
/*               MAIN               */
/*                                  */
/************************************/

int main(int argc, char *argv[])
{
    //question1();
    //question21();
    question22(argv[1]);
    //question24(argv[1]);
    //question26();

    return 0;
}


/************************************/
/*                                  */
/*            DEFINITIONS           */
/*                                  */
/************************************/

void question1()
{
    int i, wait;
    pid_t pid = fork();// returns the pid


    if (pid == 0)
    {
        printf("[fils] PID=%d, PPID=%d, v_globale=%d\n", getpid(), getppid(), globale);
        for (i=0; i!=NB_TOURS;i++)
        {
            globale +=10;
            printf("[fils] <%d> : i=%d, v_globale=%d\n", getppid(), i, globale);
            wait = (rand()*getpid())%10;
            printf("TEMPS D'ATTENTE : %d\n", wait);
            sleep(wait);
        }
        //getchar();
    }
    else
    {
        printf("[pere] PID=%d, PPID=%d, v_globale=%d\n", getpid(), getppid(), globale);
        for (i=0; i!=NB_TOURS;i++)
        {
            globale *=2;
            printf("[pere] <%d> : i=%d, v_globale=%d\n", getppid(), i, globale);
            wait = (rand()*getpid())%10;
            printf("TEMPS D'ATTENTE : %d\n", wait);
            sleep(wait);
        }
        //getchar();
    }

    printf("** Fin du processus %d, v_globale= %d**\n", getpid(), globale);

    return;
}



// Question 1.2
// L'affichage ne se fait qu'après l'exécution, seul moment où l'on peut vider
// le buffer

// Question 1.3
// fork() retourne la valeur du pid du fils, si c'est le processus pere;
// renvoie 0 si c'est le processus fils
// Le PPID du processus pere correspond au pid du processus bash

// Question 1.4
// Les valeurs finales de globale ne changent pas avec l'appel de la fonction
// sleep()

// Question 1.5
// Les temps d'attente aléatoire sont les mêmes pour les processus père et fils
// Pour ce soit aléatoire en fonction du processus, il suffit d'impliquer la
// valeur du pid ou du ppid, différents en fonction du processus



void question21()
{
    int waiting, status;
    waiting = rand()%10;
    pid_t pid = fork();

    if (pid == 0)// fils
    {
        printf("[fils] Je m'endors pour %ds...\n", waiting);
        sleep(waiting);
        printf("Je me suis reveille !\n");
    }
    else// pere
    {
        printf("[pere] En attente de son fils...\n");
        wait(&status);
    }

    printf("** Fin du process <%d> **\n", getpid());

    return;
}

void question22(char *argv)
{
    int status;
    pid_t pid = fork();
    char *arguments[] = {"echo", "Bonjour", argv, NULL};
    char *newenv[] = {NULL};

    if (pid == 0)// fils
    {
        //execve("/bin/echo", arguments, newenv);
        execlp("echo", "echo", "Bonjour", argv, (char *) NULL);
    }
    else// pere
    {
        wait(&status);
    }

    printf("** Fin du process <%d> **\n", getpid());

    return;
}


// Question 2.2
// Si succès de la commande execve, tout le code est remplacé
// cela n'arrive pas avec la commande system
// ./a.out = processus fils du terminal
// procesus pere : bash
// NULL est necessaire, c'est une sentinelle pour l'option l et v
// par défaut : commande
// p : variable PATH
// l : chaine de caractere argument ... (list)
// v : tableau d'arguments (array/vector)
// e : environnement (PATH, HOME)






void question24(char *argv)
{
    int status;
    pid_t pid = fork();
    char *arguments[] = {"./prog_fils", argv, NULL};
    char *newenv[] = {NULL};

    if (pid == 0)// fils
    {
        //execve("./prog_fils", arguments, newenv);
        execlp("./prog_fils", "./prog_fils", argv, (char *) NULL);
        //execl("./prog_fils", "./prog_fils", argv, (char *) NULL);
    }
    else
    {
        wait(&status);// necessary if we don't want to type 'enter'
    }

    return;
}



void question26()
{
    int waiting, status, choice, i;
    pid_t pid;

    printf("Input the number of child processes you want to create : ");
    scanf(" %d", &choice);

    for (i = 0; i != choice; i++)
    {
        pid = fork();
        // we don't continue the loop if it is a child
        // it must the father who has to create the other child processes
        if (pid == 0)
            break;
    }

    if (pid == 0)
    {
        waiting = abs((rand() * getpid()))%10;
        printf("[fils] Je m'endors pour %ds...\n", waiting);
        sleep(waiting);
    }
    else
    {
        printf("[pere] En attente de ses fils...\n");
        wait(&status);
        printf("Je me suis reveille !\n");
    }

    printf("** Fin du process <%d> **\n", getpid());

    return;
}
