// TP 1 PROCESSUS
// REPAIN Paul
// DUT INFO

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


int main(int argc, char *argv[])
{

    if (fork() == 0)// fils
    {
        printf("[fils] PID <%d> PPID <%d>\n", getpid(), getppid());
        printf("[fils] commande de lancement : %s\n", argv[0]);
        printf("[fils] Bonjour %s\n", argv[1]);
    }

    return 0;
}
