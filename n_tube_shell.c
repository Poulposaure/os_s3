// TP4 Communication InterProcessus IPC
// REPAIN Paul
// 2018 - 2019




#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>// wait
#include <fcntl.h>


#define DEBUG printf("<%d>\n", __LINE__);


int multi_tube_shell(int i, int argc, char *argv[], int fd[]);


int main(int argc, char *argv[])
{
    int nb_process = argc - 2, fd[2];

    if (nb_process == -1)
        return EXIT_FAILURE;
    else if (nb_process == 0)
        system(argv[1]);
    else
        return multi_tube_shell(1, argc, argv, fd);

    return EXIT_SUCCESS;
}

int multi_tube_shell(int i, int argc, char *argv[], int fd[])
{
    int fd2[2];
    pid_t pid = 1;

    if (i != 1)
        close(fd[1]);

    if (i != argc - 1)
        if (pipe(fd2) == -1) return EXIT_FAILURE;

    if (i != argc - 1)
        pid = fork();

    switch (pid)
    {
        case -1:
            perror("fork() call failed");
            return EXIT_FAILURE;

        case 0:// child
            if (i != argc - 1)
                return multi_tube_shell(i + 1, argc, argv, fd2);

        default:// father
            if (i != argc - 1)
                close(fd2[0]);

            // if not last child
            if (i != argc - 1)
                dup2(fd2[1], STDOUT_FILENO);
            //if not top parent
            if (i != 1)
                dup2(fd[0], STDIN_FILENO);

            if (i != 1)
                close(fd[0]);
            if (i != argc - 1)
                close(fd2[1]);

            system(argv[i]);

            close(STDOUT_FILENO);
            close(STDIN_FILENO);

            if (i != argc - 1)
                wait(NULL);
            exit(EXIT_SUCCESS);
    }// switch

    return EXIT_SUCCESS;
}

